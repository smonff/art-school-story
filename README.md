Un projet d'exorcisme semi-autobiographique se déroulant dans une école d'art. [Lire le manifeste](https://cybre.space/@smonff/103242632952340916).

# Règles

## Chapitres

- [Avant](https://smonff.gitlab.io/art-school-story/tags/avant/)
- [Première année](https://smonff.gitlab.io/art-school-story/tags/ann%C3%A9e-01/)
- [Deuxième année](https://smonff.gitlab.io/art-school-story/tags/ann%C3%A9e-02/)
- [Troisième année](https://smonff.gitlab.io/art-school-story/tags/ann%C3%A9e-03/)
- [Quatrième année](https://smonff.gitlab.io/art-school-story/tags/ann%C3%A9e-04/)
- [Cinquième année](https://smonff.gitlab.io/art-school-story/tags/ann%C3%A9e-05/)
- [Après](https://smonff.gitlab.io/art-school-story/tags/apr%C3%A8s/)

Chaque chapitre est composé d'une séquence de souvenirs et de rêves. Les rêves
sont artificiellement positionnés dans un espace temporel faisant sens. Ils ne
font pas l'objet d'un chapitre séparé: ainsi, ils se mélangent à la réalité.

# Crédits

Ce projet n'aurait pu se faire sans le film merveilleux [*Art School
Confidential*](https://www.imdb.com/title/tt0364955/?ref_=ttmi_tt) de Terry
Zwigoff et Daniel Clowes et en est fortement inspiré.

Voir aussi les livres
[Oeuvres](http://www.pol-editeur.com/index.php?spec=livre&ISBN=2-86744-910-3) et
[Suicide](http://www.pol-editeur.com/index.php?spec=livre&ISBN=978-2-84682-236-7)
de Edouard Levé.
