---
title: "A Propos"
author: Seb. Hu-Rillettes
date: 2020-05-15T18:15:33+02:00
draft: false
tags:
keywords:
---

Un projet d'exorcisme semi-autobiographique se déroulant dans une école d'art. [Lire le manifeste](https://cybre.space/@smonff/103242632952340916).

# Règles

Chaque chapitre est composé d'une séquence de [souvenirs](/art-school-story/tags/souvenir/) et de [rêves](/art-school-story/tags/rêve/). Les rêves
sont artificiellement positionnés dans un espace temporel faisant sens. Ils ne
font pas l'objet d'un chapitre séparé: ainsi, ils se mélangent à la réalité.

## Chapitres

- [Avant](#)
- [Première année](/art-school-story/tags/année-01/)
- [Deuxième année](/art-school-story/tags/année-02/)
- [Troisième année](#)
- [Quatrième année](/art-school-story/tags/année-04/)
- [Cinquième année](/art-school-story/tags/année-05/)
- [Après](/art-school-story/tags/après/)

# Crédits

Ce projet n'aurait pu se faire sans le film merveilleux [*Art School
Confidential*](https://www.imdb.com/title/tt0364955/?ref_=ttmi_tt) de Terry
Zwigoff et Daniel Clowes et en est fortement inspiré.

Voir aussi les livres
[Oeuvres](http://www.pol-editeur.com/index.php?spec=livre&ISBN=2-86744-910-3) et
[Suicide](http://www.pol-editeur.com/index.php?spec=livre&ISBN=978-2-84682-236-7)
de Édouard Levé.
