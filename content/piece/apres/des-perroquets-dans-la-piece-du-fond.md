---
title: "Des perroquets dans la piece du fond"
author: Seb. Hu-Rillettes
date: 2020-06-12T21:05:11+02:00
draft: false
tags:
- rêve
- après
keywords:
- perroquet
- demon
- ruines
- nid
- espace
---

J'habite dans une très grande maison que je sais partiellement en ruine dans
certaines pièces. Particulièrement, il y a une pièce au fond, qui est déjà
apparue dans de précédents rêves et que j'évite au maximum à cause de son
délabrement et sa potentielle occupation par divers *démons*.

Finalement je décide de m'aventurer dans cette direction. On trouve de très
jolis meubles anciens dans la pièce précédente, certainement de valeur. Je
rentre à reculons dans la pièce redoutée et m'attend au pire car la première
fois j'avais été accueilli par un squelette terrifiant et la dernière, le toit
manquait de tomber à cause d'infiltrations d'eau et finalement un démon en laine
de verre m'avait sauté dessus.

Finalement *la pièce du fond* est très ensoleillée et toutes les infiltrations
d'eau on séché. Il n'y a que quelques trous dans le plâtre, mais rien de
catastrophique. Je regarde un mur et quelque chose me fait sursauter de peur (un
araignée terrorisante?) mais finalement il s'agit de brindilles enchevêtrées,
qui s'avèrent être un nid.

En explorant plus la pièce, je tombe sur un couple d'oiseau qui occupent la
pièce. Au début je crois que ce sont des perruches à cause de leur tête
souriante, mais il s'avère que ce sont en réalité une variété curieuse de
perroquets. Ils ont un peu de mal à marcher, comme si ils étaient saouls et en
les regardant bien, je trouve qu'ils ressemblent à des roses. Une variété rose
clair.
