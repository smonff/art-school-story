---
title: "Poursuivi"
author: Seb. Hu-Rillettes
date: 2019-12-15T19:58:26+02:00
draft: false
tags:
- souvenir
- après
keywords:
- enseignant
- musée
- exposition
- rêve
---

Je visite l'expo Francis Bacon avec A. et je m'aperçois que l'enseignant C. est
arrivé dans l'expo en même temps que nous. Je ne l'ai pas vu depuis 13 ans et je
n'ai aucune envie de lui parler et fais exprès de ne surtout pas le rencontrer,
bien qu'il ne me reconnaîtrait sûrement pas.

Plus tard, J. me fait remarquer que tout comme *les rêves de l'école d'art me
poursuivent*, je suis tout aussi bien *poursuivi* dans la réalité.
