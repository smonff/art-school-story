---
title: "En résidence"
author: Seb. Hu-Rillettes
date: 2020-08-19T13:02:34+02:00
draft: false
tags:
- rêve
- après
keywords:
- espace
---

Je suis de retour à l'école ENSA Bourges pour une résidence d'artiste. Le
logement n'est pas terrible mais j'ai un projet très enthousiasmant bien
qu'indéfini à réaliser dans mon rêve. Je passe quelques temps à explorer
différentes options de chambres alternatives, dont une étrange mezzanine dans des
combles mais c'est un peu miteux. Je croise C. qui me propose de venir chez elle
plutôt que de rester dans le logement fourni dans le cadre de la résidence mais
j'ai en tête d'explorer un peu plus les possibilités fournies par l'institution.

Lors de mon arrivée cette fois-ci en tant qu'artiste j'ai accès à des espaces
que je n'avais jamais encore visités auparavant en tant qu'étudiant. C'est à la
fois dû au fait de mon nouveau statut et aussi au fait que je n'avais jamais osé
accéder à ces espaces, qui demandaient un peu d'interaction sociale avancée avec
des personnages clé. Je me retrouve grâce à ma curiosité dans une immense
réserve contenant la collection de machines de production vidéo et sonore de
l'école. Je découvre alors qu'il s'agit de la plus grande pièce du bâtiment. La
superficie est environ celle d'un gymnase, mais légèrement plus grande, on
touche à l'entrepôt. Le plafond doit tourner autour des 7-9
mètres. L'intégralité du lieu est peint en blanc, flambant neuf comme dans un
white cube. C'est G. qui maintient cette collection et son état est
irréprochable. Elle fait penser à un studio de tournage sauf qu'elle est
blanche.

Toutes les machines sont accrochées tête en bas sur des rails pendant du plafond
à mi-distance entre sol et plafond. De nombreux câbles avec des mousquetons
permettent d'accéder au machines pour procéder aux branchements et patchages
internes. Ici, utiliser les machines demande donc de s'harnacher afin d'accéder à
ces rails, pour se hisser jusqu'à la collection à quelques mètres du sol.

Dès que je rentre dans la réserve, il y a Philippe Z. qui zone dans la réserve
et qui a l'air un peu saoul, je n'arrive pas à savoir si il parle tout seul ou
s'adresse à moi. Finalement il s'approche et je m'attends à un contact
difficile, mais çà se passe relativement bien. Ensuite il me suivra partout en
racontant des trucs un peu drôles.

Il y a des machines extrêmement intéressante (ces machines n'existent pas toutes
en réalité même si elles sont *crédibles*), dont une avec des teintes roses et
violet foncé rappelant un peu le [pdp11/20 de Digital Equipment
Corporation](https://www.computerhistory.org/collections/catalog/X1507.98) que
j'avais vu au Computer History Museum de Montain View. Les machines sont
classées par type et ne sont pas montées dans des racks ou des étagères: chaque
machine est suspendue sur le rail, la tête en bas et l'une après l'autre. Il y a
des bancs de montage, des lecteurs divers et variés, des boites à rythmes et
synthétiseurs et toutes sortes d'autres appareils. Il y a même une
classification pour les machines de production audio-vidéo fonctionnant par
*"ondes"*. Toute les machines sont fonctionnelles et peuvent être utilisées.

Avant de sortir, je m'aperçois que l'enseignant-cinéaste Éric B. était justement
en train de monter un film assisté d'un étudiant. Je n'ai pas spécialement envie
de communiquer avec lui et aucune interaction ne se produit.
