---
title: "Un Démon Dans Les Combles"
author: Seb. Hu-Rillettes
date: 2020-01-07T04:46:08+02:00
draft: false
tags:
- rêve
- après
keywords:
- démon
- voiture
- directeur
- famille
---

J'intègre après mon premier cycle d'études une seconde école d'art. Je suis très
intéressé par cette école d'art et ai le sentiment positif que *« c'est la bonne
»*. Toutes les personnes croisées à cet endroit paraissent exceptionnellement
intéressantes et attirantes.

L'école d'art est immense. Peut être des centaines d'étudiants et divers
laboratoires au sérieux irréprochable semblent avoir des axes de recherche
extraordinairement intéressants (comprendre: ce n'est pas du tout le cas
habituellement dans les écoles d'art). Je me promène dans l'école et croise un
laboratoire d'informatique composé d'ordinateurs antiques au potentiel
intéressant. Même les petits écrans cathodiques semblent passionnants.

J'habite dans une très grande maisons au bord de la Seine (mais pas à
Paris). Cette maison a été rencontrée sous plusieurs formes dans des
rêves précédents. Il semblerait qu'elle m'ait été fournie par Mme C. du château
de Menetou-Salon où j'avais travaillé comme gardien en 2005 pendant mes études
car elle est équipée de meubles, appareils et vaisselles luxueux et me rappelant
la résidence royale. Mon déménagement comme toujours est laborieux et j'ai dû
emprunter la voiture de mon père D. afin de bouger tout un tas de brol très précieux à
mes yeux.

Suite à une altercation violente avec J. et T. de l'institut de sondage qui
s'avèrent être respectivement étudiant et professeur à l'école d'art, je me
retrouve dans une situation où je refuse de me compromettre (il y avait une
forme d'injustice dans ce qui m'était reproché). Finalement le directeur de
l'école (rappelant vaguement C. de l'institut de sondage) est dépêché sur place
et je l'insulte dans une phase d'énervement extrême, qualifiant au passage d'*«
école de merde »* l'établissement que je trouve en réalité formidable, juste par
pure provocation.

Suite à cette situation je comprends que je suis grillé et dois donc envisager
mon départ. Je suis très préoccupé par le déménagement de mon brol et la
libération de la maison (laisser aller au niveau de la vaisselle et du
ménage). Pour le déménagement, je fais appel à la voiture de D. (sa
vieille Renault 21) mais il est compliqué de l'obtenir.

Mon frère M. est présent et j'essaie de lui faire discerner l'eau qui s'échappe
des combles et notamment de la laine de verre. On observe les nombreuses fuites
provenant des combles avec M. et on s'aperçoit que la laine de verre respire.

Peu de temps après, un démon jaune au sourire terrifiant et ressemblant à ces
dragons poilus de nouvel an Chinois jaillit des combles et se jette sur nous. En
même temps, le plafond est immédiatement nettoyé de toutes ses fuites et
problèmes.

Le démon nous poursuit, mais on parvient à s'en débarrasser définitivement (pas
de souvenir de comment).

Je comprends alors que j'ai fait une erreur et souhaite vraiment rester dans
cette école d'art. Ma mère H. est également présente et me fait remarquer que la
maison et tout son équipement est vraiment exceptionnel. À ce moment là on
s'aperçoit que il y a même des lave-vaisselles.

Pendant assez longtemps, j'ai fait le deuil de cette situation et accepté ce
fait malgré que cela me rendre très triste. Mais après un moment, je décide
finalement de me confier à Mme C. et de lui avouer mon enthousiasme pour
cette école d'art exceptionnelle et ma déception d'en être exclu. Elle éclate en
sanglots car elle semble être liée à la haute administration de l'école et je
commence à pleurer aussi sous le coup de l'expression des émotions. Étant donné
la situation, je suis immédiatement réintégré.
