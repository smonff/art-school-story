---
title: "Rouler un joint à Sunny"
author: Seb. Hu-Rillettes
date: 2021-01-21T18:50:44+01:00
draft: false
tags:
- souvenir
- après
keywords:
- sunny-murray
- jazz
- joint
- bière
- concert
---

J'apprends hier soir le décès (remontant à 2017) de Sunny Murray, le mythique batteur de free-jazz.

On répétait au studio de répétition [*le Plugin*](https://www.musicvibration.com/studio-musique-studios-plug-in-603.html), un des studios de répètes les plus moisis de Paris. Du moins dans sa partie publique, c'est à dire à la cave, mais il y avait aussi un beau studio à l'étage avec le matériel pour un groupe de jazz de qualité. Aussi le moins cher dans le treizième arrondissement. Visiblement, Sunny Murray y trainait, et dans le quartier aussi.

Le gérant de ce lieu, *Michel*, nous avait donc invité à un concert qui était donné à un centre social voisin par Sunny Murray et Arthur H. pour, il me semble, des enfants malades. On se pointe donc au concert, il n'y a pas grand monde.

Un mec assez âgé commence à parler avec nous devant le centre social. Je crois que on boit des bières. Il me demande de rouler son joint car il tremble trop ce que je fais tant bien que mal. Le mec tire 3 lattes, nous laisse le joint et entre dans le centre social.

Bon finalement le concert commence et je comprends que j'ai roulé le joint de Sunny Murray ok 😱 

LE JOINT DE SUNNY MURRAY
