---
title: "Lutte Des Classes"
author: Seb. Hu-Rillettes
date: 2019-11-07T23:00:02+02:00
draft: false
tags:
- rêve
- après
keywords:
- sondage
- affrontement
- classe
- nounours
---

Une réunion à l'institut de sondage tourne à l'affrontement entre F. et moi. Un
groupe de personnes aux capacités d'expression très aisées et raffinées nous
reproche de proposer des solutions et concepts dépassés par l'état de l'art des
pratiques actuelles. Je comprends le propos et tente de défendre mon projet,
mais mes capacités d'expression orale et de persuasion ne font pas le
poids. J. qui a travaillé avec moi sur le projet est absent : il est censé être
là alors je le cherche des yeux mais ne peux le trouver.

Finalement, C., une camarade de l'école de design ([année
1](/art-school-story/tags/année-01/)) stoppe la logorrhée de dénigrement de
façon agressive en attaquant F. sur son sentiment de supériorité. Je suis un peu
étonné car C. est typiquement la personne parisienne éduquée et se sentant au
dessus des autres de par une situation familiale aisée mais exploit la situation
et me sens même un peu séduit. F. se transforme immédiatement en gros nounours
et peu à peu, la réunion professionnelle se transforme en immense fête d'école
d'art dans un village. On constatera par la suite que F. a amené un pijama
*ourson* poilu et il l'enfilera vite pour la fête, précisant qu'il est pressé
d'en avoir fini avec le présentation pour le porter.

Comme une grande fête dans un village. Il semblerait que j'ai des
responsabilités.

Je conduis le car permettant de transporter le groupe sans difficultés majeures.

Je suis obsédé par des affaires que j'ai emmenées avec moi, ou des objets que
j'ai collectés, parfois des choses insignifiantes, telles que des
papiers. Parfois, j'égare un sac ou des objets que j'ai laissé trainer, et en
emmène d'autres pour *compenser*.
