---
title: "Des Asticots à l'École d'Art"
author: Seb. Hu-Rillettes
date: 2004-05-21T10:53:26+02:00
draft: false
tags:
- rêve
- année-03
keywords:
---

Je peints des formes abstraites ressemblant à des asticots multicolores
enchevêtrés sur des petits formats en carton. L'imbrication des lignes rend le
travail extrêmement compliqué et demande une concentration importante. L'atelier
est celui du *« gymnase »* de l'ENSA Bourges.

Mon frère M. décide de m'aider car la tâche est importante. On doit alors pour
une raison de roulement d'occupation des ateliers, déménager vers une autre partie de
l'école et on est obligé d'aller dans des ateliers minuscules, sales et remplis
d'étudiant·e·s moroses et repoussant·e·s.

On trouve une place (sûrement dans un atelier [déjà vu dans le rêve lié à la
sécurité
incendie](/art-school-story/piece/annee-04/la-regulation-anti-incendie/) et on
se met au travail. J'explique en détail en quoi consiste le boulot à M. et suis
confiant sur ses capacités à réaliser ce travail vu qu'il est plutôt rigoureux.

Finalement, il ne suit pas du tout les recommandation et commence à dessiner des
lignes beaucoup moins lisses avec plein d'aspérités et qui me font un peu penser
à des contours de dragons. Je suis un peu paniqué face à l'imprévu.

Finalement je trouve que c'est assez cool et pense que c'est une opportunité
pour insérer de nouvelles formes hyper-innovants dans ma pratique artistique.
