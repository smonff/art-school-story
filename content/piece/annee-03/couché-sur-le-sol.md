---
title: "Couché Sur Le Sol"
author: Seb. Hu-Rillettes
date: 2003-11-04T21:52:53+02:00
draft: false
tags:
- souvenir
- année-03
keywords:
---

<mark>TW alcool</mark>.

On est à un vernissage à la galerie Pictura. Tout le monde parle beaucoup et est très démonstratif. Je n'ai absolument aucune idée de ce que je fais là ni de ce que je pourrais dire aux personnes.

Je m'échappe et atterris dans une rue sordide. Un peu saoul, je m'étend au sol à côté d'une sorte de transformateur électrique. Je reste là car je me sens désespéré.

Ce n'est que des années après que je comprendrais que les vernissages sont des lieux toxiques et que j'ai perdu un temps massif à les fréquenter.
