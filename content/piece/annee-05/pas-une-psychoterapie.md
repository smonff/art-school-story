---
title: "Pas Une Psychoterapie"
author: Seb. Hu-Rillettes
date: 2006-04-02T11:08:07+02:00
draft: false
tags:
- souvenir
- année-05
keywords:
---

L. dit de façon un peu hautaine à propos de la bande de boucs émissaires
étudiantes que personne n'aime:

> l'école d'art c pas une psychothérapie!

Je me sens mal parce que j'en aurais bien besoin d'une et je me suis retrouvé à
l'école d'art un peu pour çà.
