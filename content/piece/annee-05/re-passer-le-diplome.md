---
title: "Re Passer Le Diplome"
author: Seb. Hu-Rillettes
date: 2022-09-04T20:26:58+02:00
draft: false
tags:
- rêve
- année-05
keywords:
---

Le rêve commence par des voyages dans une sorte d’école d’art située dans un vieux bâtiment médiéval immense, type forteresse surplombant la ville. Peu de souvenirs de cette partie bien que je me rappelle accéder à un magnifique panorama en nageant dans une sorte de torrent coulant dans une structure en pierre. Cette partie très ancienne possède aussi de vieux rails et des grilles en métal. Je me rappelle aussi une visite dans une tour de *« la forteresse »* une ancienne cellule de moines qui était fournie à un étudiant qui avait plusieurs enfants, et il était possible pour lui de les recevoir grâce à de petits lits superposés.

Maintenant, le rêve en lui même, là où çà devient artistique... Je dois soutenir mon DNSEP auprès du jury dans [le Grenier de l’ENSA Bourges](https://dnap.ensa-bourges.fr/wp-content/gallery/camille-cathudal/_DSC2109.jpg). Ma production pour l’année a été quasi nulle, partiellement car j’ai été en voyage (notamment dans cette incroyable « forteresse-école-d-art ») où de multiples rencontres artistiques ont lieu. J'ai souffert d’absence de créativité a cause de la dépression. 

Le jury se présente une première fois à 11h: je suis totalement démuni et impréparé, donc négocie qu’ils reviennent en toute fin de journée. Iels sont un peu outrées mais acceptent tout de même. J’aime le fait d’avoir osé cette insolence, je considère celà comme un happening en soi, mais le jury, ne trouvant pas assez spectaculaire, ne le voit pas de ce cette façon.

Je me lance dans une collecte, dans mon bordel d'étudiant aux Beaux Arts, de dessins réalisés pendant l’année, sachant que il y a des choses qui tiennent la route. Je suis aussi extrêmement sceptique car cela ne reflète pas du tout la façon dont j’aimerais présenter mon parcours artistique à ce stade. Je me situe dans une pratique beaucoup plus conceptuelle et suis attristé de ne présenter que des dessins.

Je cherche desesperement certaines pièces que je n’arrive pas à retrouver. C’est comme si je revenais passer mon diplôme 10 ans après mes études et que je retrouvais mes affaires laissées en l’état.

Je parviens finalement à remplir une table en disposant dessus des dessins. Ce sont de magnifiques dessins ou peintures sur papier, faisant apparaître de grosses formes colorées, mais ils souffrent de ne pas être encadrés. J’aurais aimé retrouver des cadres au dernier moment pour les disposer au mur et montrer ma bonne volonté, mais je suis obligé de les poser sur une table moche de l’école, une espèce de carré blanc de 125 x 125 cm avec des pieds en métal répugnants. Quelques objets issus de mes perigrenations récentes sont disposés parmis les dessins. Notamment un magnifique pistolet ancien à barillet tournant, que je neutralise en recouvrant ses mécanismes internes avec de la colle blanche afin que personne ne puisse l’utiliser (bien que il n’y a pas de balles). Je trouve vraiment plus satisfaisant que ces objets divers accompagnent les dessins: il s’agit un minimum d’une installation, et pas uniquement d'une simple présentation de dessins.

Au dernier moment, après des heures d’anxiété et alors que je sais pertinemment que je ne suis pas prêt (notamment sur l'aspect du *« discours »* que je vais tenir), je déplace la table seul, en la tirant pour l’amener dans un coin un peu plus « white cube » du Grenier (c’est à dire, loin de mon capharnaüm d’atelier). Le jury est déjà la, et s’offusque un peu en mode *« holala, c’est incroyable il n’est toujours pas prêt! Quel toupet... »*. Lors du déplacement de la table, la totalité des dessins tombent au sol, ce qui anéantit mes chances de pouvoir présenter *« quelque chose »*. Je me retrouve dans la position où je ne peux que me reposer sur une bonne trouvaille et coincer le jury.

Se produit alors un phénomène étrange et salvateur: je me dédouble physiquement, avec une personnalité complètement autre (beaucoup plus sûr de moi) et commence une soutenance-happening de diplôme. Le happening consiste à donner au jury un questionnaire sur un artiste contemporain conceptuel bien connu (il s’agit d’un artiste imaginaire, mais dans le rêve le jury le reconnaîssait). Le jury trouve le concept un peu à chier, mais met de la bonne volonté quand même à remplir son questionnaire. Pendant ce temps la, « je » (le double) continue à présenter de façon très sûre de soi des idées assez intéressantes, alors que je dispose à nouveau les dessins sur la table (ceux que je ramasse par terre ne correspondent pas du tout aux dessins sélectionnés initialement. Tout à coup, j’interviens dans la performance, tout en ajustant mes dessins sur la table, pour faire remarquer que l’artiste conceptuel en question travaille sur *« les agents du monde de l’art »* (sous entendant que les personnes du jury sont des *« agents »*) et que par conscequent, nous mettons ici le jury en abîme en se rue-appropriant le travail de l’artiste du questionnaire (iels n'apprécient pas trop). Ça me semble être une idée sur-puissante, suffisant pour décrocher mon diplôme car le *« dispositif fait œuvre »*.

À ce stade peut être que mon *« double »* est devenu une vraie personne totalement distincte: en tout cas le jury trouve que tout est normal et moi j'ai l'impression que c'est un pote. 

Ensuite je me rappelle juste d’annonce confuses des résultats s’empillant et de plein de mentions *« bien »*, *« assez bien »*, etc., et quelques *« félicitations »*. Le rêve ne mentionne pas si j’ai eu mon diplôme et je crois que je m’en fous.

