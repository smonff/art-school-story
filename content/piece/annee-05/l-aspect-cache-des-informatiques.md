---
title: "L'Aspect Caché Des Informatiques"
author: Seb. Hu-Rillettes
date: 2006-02-27T15:03:37+02:00
draft: false
tags:
- souvenir
- année-05
keywords:
---

S. me montre l'éditeur Emacs dans un terminal Mac OS X. L'univers UNIX caché
derrière les belles interfaces se révèle à moi. Je ne comprends pas à quoi çà
sert mais je suis fasciné.
