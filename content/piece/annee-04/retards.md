---
title: "Retards"
author: Seb. Hu-Rillettes
date: 2022-09-16T22:46:09+02:00
draft: false
tags:
- rêve
- année-04
keywords:
---

Je rencontre F. en arrivant à l’école d’art et nous commençons à parler de nos heures de lever respectives, et comment cela nous permet d’arriver à l’heure (ou en retard) tous les matins. 

F. semble avoir un rythme très régulier: il se réveille à 6h du matin pour arriver à la première heure. Il est question de son père, qui travaille pour une banque, qui se lève à la même heure depuis toujours (et peut être encore plus tôt), ce qui lui a donné ce rythme.

Durant la conversation, alors que je pensais m’être levé suffisamment tôt, F. me fait remarquer que il est 18h30 et que par conséquent, c’est raté pour aujourd’hui (et surement pour les autres jours aussi). Cela me pose problème donc je lui demande combien d’heures il dort pas nuit et à quelle heure il se couche: je parie pour 5 à 6h mais il me répond 7 avec un coucher à 23h grand max.

Ensuite débarque une étudiante non-identifiée, ultra-excitée et possiblement saoule, qui m’annonce qu'elle vient juste d’arriver aussi, et que c’est ainsi: il y a les personnes lève tôt et celles qui se lèvent plus tard. Ça n’a l’air de lui poser aucun problème et elle m’encourage à me détacher de cela et ne pas m'en faire.

Suite à celà, des étudiants antifa (dont A. et N.) se battent avec des mecs ultra-violents. Lorsqu’ils les vainquent, ils leur tatouent des signes rouges sur la tête. Après la baston, je tente de cacher V. en me disant que il ferait mieux de ne pas être vu ici, mais finalement, on est repérés, et dans une grande confusion et excitation, décident de me tatouer un point sur l’oreille. Je m’attends à ce que ça fasse très mal mais je ne sens presque rien.

Le rêve mélange des protagonistes de l’année 01 avec les années suivante, ainsi que une personne du présent n’ayant rien à voir avec l’école d’art.
