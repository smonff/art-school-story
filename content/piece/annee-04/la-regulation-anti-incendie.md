---
title: "La Regulation Anti Incendie"
author: Seb. Hu-Rillettes
date: 2005-06-15T11:54:59+02:00
draft: false
tags:
- rêve
- année-04
keywords:
- incendie
- peinture
- étudiant
- autorité
- espace
- école
---

<mark>TW feu</mark>.

Il y a une nouvelle régulation anti-incendie à l'école d'art et du coup un mec
trop chiant vient expliquer à toutes les étudiant-e-s faisant de la peinture que
laisser des mélanges de peinture dans des gobelets en plastique est hautement
inflammable et que en conséquence, il serait dorénavant obligatoire de vider ces
gobelets tous les soirs dans l'évier. Du coup, une queue se forme aux éviers
pour se plier à ces exigences mais je tentais de négocier car *« il n'est pas
possible de travailler ainsi, parfois je garde un gobelet de mélange de couleur
pendant 1 mois! »*.

Il faut savoir que en général, pour peindre à l'acrylique, on n'utilise pas de
palette: il est plus simple de diluer un peu de peinture dans de l'eau jusqu'à
atteindre la consistance souhaitée, puis de vaporiser de l'eau de temps en temps
pour que le mélange ne sèche pas. En utilisant des films plastique, on peut
ainsi garder une couleur pendant quelques temps.

Je trouvais cette règle complètement injustifiée et en plus cela engendrait un gâchis et
des coûts disproportionnés pour les étudiant-e-s.

Plus tard, je suis le type qui inspectait des pièces abandonnées dans l'école
et il y avait plein de choses intéressantes, notamment un toit qui fuyait sur
une machine en marche. Au début il ne voulait pas trop me croire à propos du
fait que la machine était en marche, mais finalement en regardant un système
d'engrenages, on voyait bien que elle était en action.
