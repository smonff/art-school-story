---
title: "Scotché"
author: Seb. Hu-Rillettes
date: 2005-03-28T21:29:08+02:00
draft: false
tags:
- souvenir
- année-04
keywords:
---

<mark>TW corps et maltraitance</mark>.

Quand j'étais à l'école d'art, le camarade N. a voulu faire un film avec dedans une sorte de *« scotch man »* et m'a proposé d'interpréter le rôle. Ça consistait à se faire recouvrir de scotch intégralement (sur la peau) et ensuite à déambuler dans une sorte de loft et un jardin. Déjà çà peut paraître une mauvaise idée vue l'absence totale de sens, mais surtout: le ruban adhésif choisi a été du scotch d'électricien.

C'est doublement une mauvaise idée car le scotch d'électricien, lorsque on le chauffe, se rétracte (je crois çà sert à compresser des fils electriques ensemble). Donc alors que trois ou quatre personnes me recouvraient de ces fines bandelettes, après genre 15 minutes, avec la chaleur du corps, j'ai trouvé que j'avais du mal à respirer. Bref, ça serrait. Et çà ne s'arrangeait pas avec le temps.

Je ne sais plus trop comment ça s'était fini (pas trop mal, pas aux urgences), on a finalement coupé les scotchs et ça m'a libéré, après avoir shooté son film de merde d'étudiant.

Après quand j'y repense, je crois que cet artiste a développé sa pratique uniquement sur les adhésifs: ça aura quand même servi à quelque chose.

Je n'arrive pas trop à analyser rétrospectivement si j'étais d'accord pour faire çà ou si je l'ai fait contre mon gré.

Tout d'abord publié sur https://cybre.space/@smonff/103242632952340916 .
