---
title: "Mort De Rire a Bandits Mages"
author: Seb. Hu-Rillettes
date: 2002-04-16T18:12:37+02:00
draft: false
tags:
- souvenir
- année-02
keywords:
- vodka
- rire
- étudiant
---

**CW alcool.**

Avec E. dans le théâtre Jacques Coeur lors de la nuit des projections des films
d'étudiants, on boit de la vodka à la bouteille et on a une crise de fou rire parce que les films sont un peu pourris.

On n'arrive pas à s'arrêter.
